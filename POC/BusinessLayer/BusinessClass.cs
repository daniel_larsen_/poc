﻿using BusinessLayer.AOP;
using NLog;
using System;
using System.Threading.Tasks;

namespace BusinessLayer
{
    internal class BusinessClass : IBusinessFacade
    {
        private readonly ILogger _logger;

        public BusinessClass(ILogger logger)
        {
            _logger = logger;
        }

        public Result<object> DoSomething(bool throwError = false)
        {
            if (throwError)
            {
                throw new ArgumentException("I didn't do anything!");
            }
            else
            {
                _logger.Info("I did something!");
            }

            return new Result<object>(true, null);
        }

        public async Task<Result<bool>> DoSomethingElse(bool throwError = false)
        {
            if (throwError)
            {
                throw new ArgumentException("I didn't do anything else!");
            }
            else
            {
                await Task.Run(() => _logger.Info("I did something else!"));
            }

            return new Result<bool>(true, true);
        }
    }
}