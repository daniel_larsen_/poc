﻿using Ninject.Extensions.Interception;
using NLog;
using System;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace BusinessLayer.AOP
{
    public class LogInterceptor : IInterceptor
    {
        private readonly ILogger _logger;

        public LogInterceptor(ILogger logger)
        {
            _logger = logger;
        }

        public void Intercept(IInvocation invocation)
        {
            _logger.Info($"Called method {invocation.Request.Method.Name}");

            try
            {
                //Synchronous methods will execute at this point.
                invocation.Proceed();
            }
            catch (Exception ex)
            {
                _logger.Error("An unhandled exception occurred synchronously: " + ex);
                invocation.ReturnValue = ConstructFailureResultOfMethod(invocation.Request.Method);
                return;
            }

            //If it's an async method, it hasn't executed yet. Handle that as well.
            if (invocation.ReturnValue is Task task)
            {
                HandleAsyncExceptions(invocation, (dynamic)task);
            }

            _logger.Info($"Leaving method {invocation.Request.Method.Name}");
        }

        private static dynamic ConstructFailureResultOfMethod(MethodInfo methodInfo) {
            Type resultType = typeof(Result<>);

            var genericResult = resultType.MakeGenericType(methodInfo.ReturnType.GenericTypeArguments.First());

            return Activator.CreateInstance(genericResult, false, null);
        }

        private static dynamic ConstructFailureResultOfMethodAsync(MethodInfo methodInfo)
        {
            Type resultType = typeof(Result<>);

            var taskTypeArgument = methodInfo.ReturnType.GenericTypeArguments.First();
            var resultTypeArgument = taskTypeArgument.GenericTypeArguments.First();


            Type taskType = typeof(Task<>);
            var genericTask = taskType.MakeGenericType(taskTypeArgument);

            //var generatedTask = Activator.CreateInstance(genericTask);


            var genericResult = resultType.MakeGenericType(resultTypeArgument);

            //var method = genericTask.GetMethod("FromResult");

            var taskCompletionSourceType = typeof(TaskCompletionSource<>);
            var genericTaskCompletionSource = taskCompletionSourceType.MakeGenericType(taskTypeArgument);

            var taskSource = Activator.CreateInstance(genericTaskCompletionSource);
            var setResultMethod = genericTaskCompletionSource.GetMethod("SetResult");

            setResultMethod.Invoke(taskSource, new object[] { Activator.CreateInstance(genericResult, false, null) });

            return taskSource.GetType().GetProperty("Task").GetValue(taskSource);

            //method.Invoke(null, new object[] { Activator.CreateInstance(genericResult, false, null) });


            //return ((dynamic)genericTask).FromResult(Activator.CreateInstance(genericResult, false, null));



            //return Task.FromResult(Activator.CreateInstance(genericResult, false, null));
        }

        private async Task HandleAsyncExceptions(IInvocation invocation, Task source)
        {
            try
            {
                await source.ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                _logger.Error("An unhandled exception occurred asynchronously: " + ex);

                invocation.ReturnValue = ConstructFailureResultOfMethodAsync(invocation.Request.Method);
            }
        }
    }
}