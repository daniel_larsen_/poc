﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLayer
{
    public class Result<T>
    {
        public bool Succeeded { get; private set; }
        public T Content { get; private set; }

        public Result(bool succeeded, T content) {
            this.Succeeded = succeeded;
            this.Content = content;
        }
    }
}
