﻿using BusinessLayer.AOP;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public interface IBusinessFacade
    {
        Result<object> DoSomething(bool throwError = false);
        Task<Result<bool>> DoSomethingElse(bool throwError = false);
    }
}