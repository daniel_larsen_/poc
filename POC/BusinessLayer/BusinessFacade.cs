﻿using NLog;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public class BusinessFacade : IBusinessFacade
    {
        private readonly BusinessClass businessClass;

        public BusinessFacade(ILogger logger)
        {
            businessClass = new BusinessClass(logger);
        }

        public Result<object> DoSomething(bool throwError = false)
        {
            return businessClass.DoSomething(throwError);
        }

        public async Task<Result<bool>> DoSomethingElse(bool throwError = false)
        {
            return await businessClass.DoSomethingElse(throwError);
        }
    }
}