using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BusinessLayer;
using BusinessLayer.AOP;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Ninject.Extensions.Interception;
using Ninject.Extensions.Conventions;
using Microsoft.Extensions.Logging;
using Ninject;
using Ninject.Extensions.Interception.Infrastructure.Language;
using NLog;

namespace AOP
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            var kernel = new StandardKernel();

            kernel.Bind<NLog.ILogger>().ToMethod(p => NLog.LogManager.GetCurrentClassLogger(p.Request.Target.Member.DeclaringType));

            kernel.Bind(x => x.FromAssembliesMatching("BusinessLayer.*")
                        .SelectAllClasses()
                        .BindAllInterfaces()
                        .Configure(b => b.Intercept().With<LogInterceptor>()));

            APIContext.Kernel = kernel;
        }
    }
}
