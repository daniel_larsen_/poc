﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BusinessLayer;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Ninject;

namespace AOP.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<WeatherForecastController> _logger;

        public WeatherForecastController(ILogger<WeatherForecastController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public async Task Get(bool throwError = false, bool throwErrorAsync = false)
        {
            var someClass = APIContext.Kernel.Get<IBusinessFacade>();
            
            var doSomethingResult = someClass.DoSomething(throwError);
            if (doSomethingResult.Succeeded)
            {
                _logger.LogInformation("DoSomething went well!");
            }
            else
            {
                _logger.LogInformation("DoSomething failed for some reason...");
            }

            await someClass.DoSomethingElse(throwErrorAsync);
        }
    }
}